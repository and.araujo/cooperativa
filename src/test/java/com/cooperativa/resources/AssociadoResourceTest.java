package com.cooperativa.resources;

import com.cooperativa.domains.Associado;
import com.cooperativa.services.AssociadoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AssociadoResourceTest {

    private static final int ID = 1;
    private static final String NOME = "Teste";
    private static final String CPF = "045.658.749-89";

    private Optional<Associado> optionalAssociado;

    private Associado associado;

    @InjectMocks
    private AssociadoResource resource;

    @Mock
    private AssociadoService service;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        this.startAssociado();
    }

    @Test
    void quandoBuscarPorIdRetorneSucesso() {
        Mockito.when(service.buscarPorId(Mockito.anyInt())).thenReturn(associado);
        ResponseEntity<?> response = resource.buscarPorId(ID);
        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test
    void quandoListarTodosEntaoRetorneListaAssociados() {
        Mockito.when(service.listarTodos()).thenReturn(List.of(associado));
        ResponseEntity<?> response = resource.listarTodos();
        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getBody());
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);

    }

    @Test
    void cadastrar() throws URISyntaxException {
        Mockito.when(service.insert( Mockito.any() )).thenReturn(associado);
        ResponseEntity<?> response = resource.cadastrar(associado);
        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getBody());

    }

    private void startAssociado(){
        associado = new Associado(ID, NOME, CPF);
        optionalAssociado = Optional.of( new Associado(ID, NOME, CPF) );
        //Associado a2 = new Associado(null, "Sheila", "01234567890");
        //Associado a3 = new Associado(null, "Vanessa", "98765432109");
    }
}