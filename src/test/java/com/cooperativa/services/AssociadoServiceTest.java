package com.cooperativa.services;

import com.cooperativa.domains.Associado;
import com.cooperativa.exceptions.service.ObjectNotFoundException;
import com.cooperativa.repositories.AssociadoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AssociadoServiceTest {

    private static final int ID = 1;
    private static final String NOME = "Teste";

    private static final String CPF = "045.658.749-89";

    @InjectMocks
    private  AssociadoService associadoService;

    @Mock
    private AssociadoRepository associadoRepository;

    private Optional<Associado> optionalAssociado;

    private Associado associado;



    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        this.startAssociado();
    }

    @Test
    void quandoListarTodosEntaoRetorneListaAssociados() {
        Mockito.when(associadoRepository.findAll()).thenReturn(List.of(associado));

        List<Associado> response = associadoService.listarTodos();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(Associado.class, response.get(0).getClass());
        assertEquals(ID, response.get(0).getId());
        assertEquals(NOME, response.get(0).getNome());
        assertEquals(CPF, response.get(0).getCpf());
    }

    @Test
    void quandoBuscarPorIdEntaoRetorneInstanciaAssociado() {
        Mockito.when(associadoRepository.findById( Mockito.anyInt() )).thenReturn(optionalAssociado);
        Associado a = associadoService.buscarPorId(ID);
        Assertions.assertNotNull(a);
        Assertions.assertEquals(Associado.class, a.getClass());
        Assertions.assertEquals(ID, a.getId());
    }

    @Test
    void quandoBuscarPorIdEntaoRetornaObjetoNaoEncontrado() {
        Mockito.when(associadoRepository.findById( Mockito.anyInt() )).thenThrow( new ObjectNotFoundException("Objeto não encontrado! Id: "+ ID + ", Tipo: " + Associado.class.getName()));
        try{
            associadoService.buscarPorId(ID);
        }catch (Exception e){
            assertEquals(ObjectNotFoundException.class, e.getClass());
            String msg = "Objeto não encontrado! Id: "+ ID + ", Tipo: " + Associado.class.getName();
            assertEquals(msg, e.getMessage());
        }
    }

    @Test
    void quandoInserirRetorneSucesso() {
        Mockito.when(associadoRepository.save(Mockito.any())).thenReturn(associado);

        Associado response = associadoService.insert(associado);
        Assertions.assertNotNull(response);
        Assertions.assertEquals(Associado.class, response.getClass());
        Assertions.assertEquals(ID, response.getId());
        Assertions.assertEquals(NOME, response.getNome());
        Assertions.assertEquals(CPF, response.getCpf());

    }

    private void startAssociado(){
        associado = new Associado(ID, NOME, CPF);
        optionalAssociado = Optional.of( new Associado(ID, NOME, CPF) );
        //Associado a2 = new Associado(null, "Sheila", "01234567890");
        //Associado a3 = new Associado(null, "Vanessa", "98765432109");
    }
}