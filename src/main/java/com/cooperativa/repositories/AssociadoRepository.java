package com.cooperativa.repositories;

import com.cooperativa.domains.Associado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface AssociadoRepository extends JpaRepository<Associado, Serializable> {

}
