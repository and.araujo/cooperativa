package com.cooperativa.repositories;

import com.cooperativa.domains.Pauta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface PautaRepository extends JpaRepository<Pauta, Serializable> {

}
