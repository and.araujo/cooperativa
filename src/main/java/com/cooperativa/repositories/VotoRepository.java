package com.cooperativa.repositories;

import com.cooperativa.domains.Associado;
import com.cooperativa.domains.Pauta;
import com.cooperativa.domains.Sessao;
import com.cooperativa.domains.Voto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface VotoRepository extends JpaRepository<Voto, Serializable> {

    Voto findOneByAssociadoAndPauta(Associado associado, Pauta pauta);

    Integer countByPautaAndSessaoAndVotoDescricao(Pauta pauta, Sessao sessao, String votoDescricao);
}
