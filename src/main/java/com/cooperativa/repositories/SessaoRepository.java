package com.cooperativa.repositories;

import com.cooperativa.domains.Sessao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface SessaoRepository extends JpaRepository<Sessao, Serializable> {

}
