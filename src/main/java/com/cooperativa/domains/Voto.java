package com.cooperativa.domains;

import com.cooperativa.enums.TipoVotoEnum;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "Voto")
public class Voto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "sessao_id")
    private Sessao sessao;

    @ManyToOne
    @JoinColumn(name = "associado_id")
    private Associado associado;

    @ManyToOne
    @JoinColumn(name = "pauta_id")
    private Pauta pauta;

    private String votoDescricao;

    public Voto() {
    }

    public Voto(Integer id, Sessao sessao, Associado associado, TipoVotoEnum tipoVotoEnum) {
        this.id = id;
        this.sessao = sessao;
        this.associado = associado;
        this.pauta = sessao.getPauta();
        this.votoDescricao = tipoVotoEnum.getDescricao();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sessao getSessao() {
        return sessao;
    }

    public void setSessao(Sessao sessao) {
        sessao = sessao;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }

    public String getVotoDescricao() {
        return votoDescricao;
    }

    public void setVotoDescricao(String voto) {
        this.votoDescricao = voto;
    }
}
