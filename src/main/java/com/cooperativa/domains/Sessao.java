package com.cooperativa.domains;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Sessao")
public class Sessao implements Serializable {

        private static final long serialVersionUID = 1L;

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        @Column(name = "tempoAbertaMinutos")
        private Integer tempoAberta;

        private Date dtHoraAbertura;

        private Date dtHoraFechamento;

        @ManyToOne
        @JoinColumn(name = "pauta_id")
        private Pauta pauta;

        public Sessao() {
        }

        public Sessao(Integer id, Integer tempoAberta, Pauta pauta, Date dtHoraAbertura, Date dtHoraFechamento) {
                this.id = id;
                this.tempoAberta = tempoAberta;
                this.dtHoraAbertura = dtHoraAbertura;
                this.dtHoraFechamento = dtHoraFechamento;
                this.pauta = pauta;
        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getTempoAberta() {
                return tempoAberta;
        }

        public void setTempoAberta(Integer tempoAberta) {
                this.tempoAberta = tempoAberta;
        }

        public Date getDtHoraAbertura() {
                return dtHoraAbertura;
        }

        public void setDtHoraAbertura(Date dtHoraAbertura) {
                this.dtHoraAbertura = dtHoraAbertura;
        }

        public Pauta getPauta() {
                return pauta;
        }

        public void setPauta(Pauta pauta) {
                this.pauta = pauta;
        }

        public Date getDtHoraFechamento() {
                return dtHoraFechamento;
        }

        public void setDtHoraFechamento(Date dtHoraFechamento) {
                this.dtHoraFechamento = dtHoraFechamento;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Sessao sessao = (Sessao) o;
                return Objects.equals(id, sessao.id);
        }

        @Override
        public int hashCode() {
                return Objects.hash(id);
        }
}
