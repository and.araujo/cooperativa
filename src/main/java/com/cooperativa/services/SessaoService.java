package com.cooperativa.services;

import com.cooperativa.exceptions.service.ObjectNotFoundException;
import com.cooperativa.domains.Sessao;
import com.cooperativa.dto.SessaoDto;
import com.cooperativa.repositories.PautaRepository;
import com.cooperativa.repositories.SessaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SessaoService {

    @Autowired
    SessaoRepository sessaoRepository;

    @Autowired
    PautaRepository pautaRepository;

    public List<Sessao> listarTodos(){
        List<Sessao> lista = sessaoRepository.findAll();
        return lista;
    }

    public Sessao buscarPorId(Integer id){
        Optional<Sessao> optional = sessaoRepository.findById(id);
        Sessao s = optional.get();
        if(optional.isEmpty() || (optional == null) ){
            throw new ObjectNotFoundException("Objeto não encontrado! Id: "+id + ", Tipo: " + Sessao.class.getName());
        }
        return optional.orElse(null);
    }

    public Sessao abrirSessao(SessaoDto sessaoDto) {
        Sessao s = new Sessao();
        s.setId(null);
        if(sessaoDto.getTempoAberta() == null){
            s.setTempoAberta(1);
        }else{
            s.setTempoAberta(sessaoDto.getTempoAberta());
        }
        s.setPauta(pautaRepository.findById(sessaoDto.getPautaId()).get());
        Date dataHoraAbertura = new Date();
        s.setDtHoraAbertura(dataHoraAbertura);
        s.setDtHoraFechamento(this.somarminutos(dataHoraAbertura, s.getTempoAberta()));
        return sessaoRepository.save(s);
    }

    public Date somarminutos(Date data, Integer min){
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        Format formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        cal.add(Calendar.MINUTE, min);
        formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        return cal.getTime();
    }


}
