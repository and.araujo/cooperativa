package com.cooperativa.services;

import com.cooperativa.domains.Associado;
import com.cooperativa.domains.Pauta;
import com.cooperativa.domains.Sessao;
import com.cooperativa.domains.Voto;
import com.cooperativa.enums.TipoVotoEnum;
import com.cooperativa.repositories.AssociadoRepository;
import com.cooperativa.repositories.PautaRepository;
import com.cooperativa.repositories.SessaoRepository;
import com.cooperativa.repositories.VotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Service
public class BaseDadosService {

    @Autowired
    private AssociadoRepository associadoRepository;

    @Autowired
    private PautaRepository pautaRepository;

    @Autowired
    private SessaoRepository sessaoRepository;

    @Autowired
    private VotoRepository votoRepository;


    public void instanciarDados(){
        Associado a1 = new Associado(null, "André", "02365360521");
        Associado a2 = new Associado(null, "Sheila", "01234567890");
        Associado a3 = new Associado(null, "Vanessa", "98765432109");
        associadoRepository.saveAll(Arrays.asList(a1,a2,a3));

        Pauta p1 = new Pauta(null,"Eleição","Novo diretor da cooperativa");
        Pauta p2 = new Pauta(null,"Contribuição","Aumento da contribuição dos cooperados");
        pautaRepository.saveAll( Arrays.asList(p1,p2) );

        Date d1 = new Date();
        Integer minS1 = 5;
        Sessao s1 = new Sessao(null, minS1, p1, d1, this.somarminutos(d1, minS1) );
        Sessao s2 = new Sessao(null, 120, p2, d1, this.somarminutos(d1, 1140));
        sessaoRepository.saveAll( Arrays.asList(s1,s2) );

        Voto v1 = new Voto(null, s1, a1, TipoVotoEnum.SIM);
        Voto v2 = new Voto(null, s1, a2, TipoVotoEnum.NAO);
        //Voto v3 = new Voto(null, s1, a3, TipoVotoEnum.SIM);
        Voto v4 = new Voto(null, s2, a1, TipoVotoEnum.SIM);
        Voto v5 = new Voto(null, s2, a2, TipoVotoEnum.SIM);
        Voto v6 = new Voto(null, s2, a3, TipoVotoEnum.SIM);
//		votoRepository.saveAll(Arrays.asList(v1, v2, v3, v4, v5, v6));
        votoRepository.saveAll(Arrays.asList(v1, v2, v4, v5, v6));
    }

    public Date somarminutos(Date data, Integer min){
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        Format formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        cal.add(Calendar.MINUTE, min);
        formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
        return cal.getTime();
    }

}
