package com.cooperativa.services;

import com.cooperativa.exceptions.service.ObjectNotFoundException;
import com.cooperativa.domains.Associado;
import com.cooperativa.domains.Pauta;
import com.cooperativa.domains.Sessao;
import com.cooperativa.domains.Voto;
import com.cooperativa.dto.VotoDto;
import com.cooperativa.enums.TipoVotoEnum;
import com.cooperativa.repositories.PautaRepository;
import com.cooperativa.repositories.VotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VotoService {

    @Autowired
    VotoRepository votoRepository;

    @Autowired
    private AssociadoService associadoService;

    @Autowired
    private SessaoService sessaoService;

    @Autowired
    private PautaRepository pautaRepository;

    public List<Voto> listarTodos(){
        List<Voto> lista = votoRepository.findAll();
        return lista;
    }

    public Voto buscarPorId(Integer id){
        Optional<Voto> optional = votoRepository.findById(id);
        if(optional.isEmpty() || (optional == null) ){
            throw new ObjectNotFoundException("Objeto não encontrado! Id: "+id + ", Tipo: " + Voto.class.getName());
        }
        //return optional;
        return optional.orElse(null);
    }

    public Voto votar(VotoDto votoDto) throws Exception {
        Voto v = new Voto();
        Associado associado = associadoService.buscarPorId(votoDto.getAssociadoId());
        Sessao sessao = sessaoService.buscarPorId(votoDto.getSessaoId());
        Pauta pauta = pautaRepository.findById(sessao.getPauta().getId()).get();
        //List<Voto> associadoJaVotou = votoRepository.findOneByAssociadoAndPauta(associado, pauta);
        Voto associadoJaVotou = votoRepository.findOneByAssociadoAndPauta(associado, pauta);
        if (associadoJaVotou != null){
            System.out.println("associado null");
            throw new Exception("Já existe voto computado para o associado: " + associado.getNome() + " na pauta: " + pauta.getTitulo());
        }
        if(votoDto.getTipoVotoDescricao().equals("NÃO")
            ||votoDto.getTipoVotoDescricao().equals("Não")
            ||votoDto.getTipoVotoDescricao().equals("não")
            || votoDto.getTipoVotoDescricao().equals("NAO")
            || votoDto.getTipoVotoDescricao().equals("Nao")
            || votoDto.getTipoVotoDescricao().equals("nao")
        ){
            votoDto.setTipoVotoDescricao(TipoVotoEnum.NAO.getDescricao());
        }
        if (
            votoDto.getTipoVotoDescricao().equals("SIM")
            || votoDto.getTipoVotoDescricao().equals("Sim")
            || votoDto.getTipoVotoDescricao().equals("sim")
        ){
            votoDto.setTipoVotoDescricao(TipoVotoEnum.SIM.getDescricao());
        }
        TipoVotoEnum tve = TipoVotoEnum.getTipoVotoPorDescricao( votoDto.getTipoVotoDescricao() );
        Voto voto = new Voto(null, sessao, associado, TipoVotoEnum.getTipoVotoPorDescricao( votoDto.getTipoVotoDescricao()) );
        return votoRepository.save( new Voto(null, sessao, associado, TipoVotoEnum.getTipoVotoPorDescricao( votoDto.getTipoVotoDescricao()) ) );

    }


    public Integer contabilizarVotos(Pauta pauta, Sessao sessao, String descricaoVoto){
        return votoRepository.countByPautaAndSessaoAndVotoDescricao(pauta,sessao,descricaoVoto);
    }



}
