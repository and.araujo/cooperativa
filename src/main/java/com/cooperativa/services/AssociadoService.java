package com.cooperativa.services;

import com.cooperativa.exceptions.service.ObjectNotFoundException;
import com.cooperativa.domains.Associado;
import com.cooperativa.repositories.AssociadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AssociadoService {

    @Autowired
    AssociadoRepository associadoRepository;

    public List<Associado> listarTodos(){
        List<Associado> lista = associadoRepository.findAll();
        return lista;
    }

    public Associado buscarPorId(Integer id){
        Optional<Associado> optional = associadoRepository.findById(id);
        if(optional.isEmpty() || (optional == null) ){
            throw new ObjectNotFoundException("Objeto não encontrado! Id: "+id + ", Tipo: " + Associado.class.getName());
        }
        /*
        optionalCategoria.orElseThrow(
                ()-> new ObjectNotFoundException(
                        "Objeto não encontrado! Id: "+id + ", Tipo: " + Categoria.class.getName()
                )
        );
        */
        return optional.orElse(null);
    }


    public Associado insert(Associado associado) {
        //associado.setId(null);
        return associadoRepository.save(associado);
    }
}
