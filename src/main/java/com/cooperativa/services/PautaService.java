package com.cooperativa.services;

import com.cooperativa.exceptions.service.ObjectNotFoundException;
import com.cooperativa.domains.Pauta;
import com.cooperativa.domains.Sessao;
import com.cooperativa.enums.TipoVotoEnum;
import com.cooperativa.repositories.PautaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PautaService {

    @Autowired
    PautaRepository pautaRepository;

    @Autowired
    SessaoService sessaoService;

    @Autowired
    VotoService votoService;

    public List<Pauta> listarTodos(){
        List<Pauta> lista = pautaRepository.findAll();
        return lista;
    }

    public Pauta buscarPorId(Integer id){
        Optional<Pauta> optional = pautaRepository.findById(id);
        if(optional.isEmpty() || (optional == null) ){
            throw new ObjectNotFoundException("Objeto não encontrado! Id: "+id + ", Tipo: " + Pauta.class.getName());
        }
//        return optional;
        return optional.orElse(null);
    }


    public Pauta salvar(Pauta pauta) {
        pauta.setId(null);
        return pautaRepository.save(pauta);
    }

    public void resultado(Integer pautaId){
        Optional<Pauta> p = pautaRepository.findById(pautaId);

    }

    public Pauta contabilizarResultado(Integer id) throws Exception {
        Sessao sessao = sessaoService.buscarPorId(id);
        Pauta pauta = this.buscarPorId(sessao.getPauta().getId());

        Date d = new Date();
        if( (sessao.getDtHoraFechamento().compareTo(d)>0) || (sessao.getDtHoraFechamento().compareTo(d)==0) ){
            SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");
            String data = sdfData.format(sessao.getDtHoraFechamento());
            String hora = sdfHora.format(sessao.getDtHoraFechamento());
            throw new Exception("Sessão ainda não está encerrada! A contabilização dos votos só pode ocorrer após o fim da sessão, que ocorrerá em "+ data + " às " + hora);
        }

        Integer totalVotosSim = votoService.contabilizarVotos(pauta,sessao, TipoVotoEnum.SIM.getDescricao());
        Integer totalVotosNao = votoService.contabilizarVotos(pauta,sessao,TipoVotoEnum.NAO.getDescricao());
        pauta.setTotalVotosSim(totalVotosSim);
        pauta.setTotalVotosNao(totalVotosNao);

        if(totalVotosSim>totalVotosNao) pauta.setResultado(TipoVotoEnum.SIM.getDescricao());
        if(totalVotosSim<totalVotosNao) pauta.setResultado(TipoVotoEnum.NAO.getDescricao());
        if(totalVotosSim==totalVotosNao) pauta.setResultado("Empate");

        return pautaRepository.save(pauta);
    }
}
