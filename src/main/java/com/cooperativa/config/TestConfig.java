package com.cooperativa.config;

import com.cooperativa.services.BaseDadosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestConfig {

    @Autowired
    private BaseDadosService baseDadosService;

    @Bean
    public boolean instanciarDados(){
        baseDadosService.instanciarDados();
        return true;
    }
}
