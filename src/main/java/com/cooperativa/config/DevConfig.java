package com.cooperativa.config;

import com.cooperativa.services.BaseDadosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class DevConfig {

    @Autowired
    private BaseDadosService baseDadosService;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String instanciaDados;

    @Bean
    public boolean instanciarDados(){
        if( !"create".equals(instanciaDados) ){
            return false;
        }
        baseDadosService.instanciarDados();
        return true;
    }
}
