package com.cooperativa.resources;

import com.cooperativa.domains.Sessao;
import com.cooperativa.dto.SessaoDto;
import com.cooperativa.services.SessaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "/sessoes")
public class SessaoResource {

    @Autowired
    private SessaoService sessaoService;

    @RequestMapping(value = "/buscarPorId", method = RequestMethod.GET)
    public ResponseEntity<?> buscarPorId(@RequestParam(name = "id") Integer id) throws Exception {
        Sessao obj = sessaoService.buscarPorId(id);
        return ResponseEntity.ok().body(obj);
    }

    @RequestMapping(value = "/listarTodos", method = RequestMethod.GET)
    public ResponseEntity<?> listarTodos(){
        List<Sessao> lista = sessaoService.listarTodos();
        return ResponseEntity.ok().body(lista);
    }

    @RequestMapping(value = "/abrirSessao", method = RequestMethod.POST)
    public ResponseEntity<?> abrirSessao(@RequestBody SessaoDto sessaoDto) throws URISyntaxException {
        Sessao sessao = sessaoService.abrirSessao(sessaoDto);
        //sessao = sessaoService.salvar(sessao);
        String urlString = ServletUriComponentsBuilder.fromCurrentContextPath().path("pautas/buscarPorId?id={id}").buildAndExpand(sessao.getId()).toString();
        URI uri = new URI(urlString);
        return ResponseEntity.created(uri).body(sessao);
    }


}
