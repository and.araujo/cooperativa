package com.cooperativa.resources;

import com.cooperativa.domains.Pauta;
import com.cooperativa.services.PautaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "/pautas")
public class PautaResource {

    @Autowired
    private PautaService pautaService;


    @RequestMapping(value = "/buscarPorId", method = RequestMethod.GET)
    public ResponseEntity<?> buscarPorId(@RequestParam(name = "id") Integer id) throws Exception {
        Pauta obj = pautaService.buscarPorId(id);
        return ResponseEntity.ok().body(obj);
    }

    @RequestMapping(value = "/listarTodos", method = RequestMethod.GET)
    public ResponseEntity<?> listarTodos(){
        List<Pauta> lista = pautaService.listarTodos();
        return ResponseEntity.ok().body(lista);
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public ResponseEntity<?> cadastrar(@RequestBody Pauta pauta) throws URISyntaxException {
        pauta = pautaService.salvar(pauta);
        String urlString = ServletUriComponentsBuilder.fromCurrentContextPath().path("pautas/buscarPorId?id={id}").buildAndExpand(pauta.getId()).toString();
        URI uri = new URI(urlString);
        return ResponseEntity.created(uri).body(pauta);
    }

    @RequestMapping(value = "/contabilizarResultado", method = RequestMethod.POST)
    public ResponseEntity<?>  contabilizarResultado(@RequestParam(name = "id") Integer id) throws Exception {
        Pauta pauta = pautaService.contabilizarResultado(id);
        String urlString = ServletUriComponentsBuilder.fromCurrentContextPath().path("pautas/buscarPorId?id={id}").buildAndExpand(pauta.getId()).toString();
        URI uri = new URI(urlString);
        return ResponseEntity.created(uri).body(pauta);
    }


}
