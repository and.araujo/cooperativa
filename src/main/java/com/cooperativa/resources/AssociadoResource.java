package com.cooperativa.resources;

import com.cooperativa.domains.Associado;
import com.cooperativa.services.AssociadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping(value = "/associados")
public class AssociadoResource {

    @Autowired
    private AssociadoService associadoService;


    @RequestMapping(value = "/buscarPorId", method = RequestMethod.GET)
    public ResponseEntity<?> buscarPorId(@RequestParam(name = "id") Integer id){
        Associado obj = associadoService.buscarPorId(id);
        return ResponseEntity.ok().body(obj);
    }

    @RequestMapping(value = "/listarTodos", method = RequestMethod.GET)
    public ResponseEntity<?> listarTodos(){
        List<Associado> lista = associadoService.listarTodos();
        return ResponseEntity.ok().body(lista);
    }

    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public ResponseEntity<?> cadastrar(@RequestBody Associado associado) throws URISyntaxException {
        associado = associadoService.insert(associado);
        String uriString = ServletUriComponentsBuilder.fromCurrentContextPath().path("associados/buscarPorId?id={id}").buildAndExpand(associado.getId()).toString();
        URI uri = new URI(uriString);
        return ResponseEntity.created(uri).body(associado);
    }


}
