package com.cooperativa.resources;

import com.cooperativa.domains.Voto;
import com.cooperativa.dto.VotoDto;
import com.cooperativa.services.VotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/votos")
public class VotoResource {

    @Autowired
    private VotoService votoService;



    @RequestMapping(value = "/buscarPorId", method = RequestMethod.GET)
    public ResponseEntity<?> buscarPorId(@RequestParam(name = "id") Integer id){
        Voto obj = votoService.buscarPorId(id);
        return ResponseEntity.ok().body(obj);
    }

    @RequestMapping(value = "/listarTodos", method = RequestMethod.GET)
    public ResponseEntity<?> listarTodos(){
        List<Voto> lista = votoService.listarTodos();
        return ResponseEntity.ok().body(lista);
    }

    @RequestMapping(value = "/votar", method = RequestMethod.POST)
    public ResponseEntity<?> votar(@RequestBody VotoDto votoDto) throws Exception {
        Voto voto = votoService.votar(votoDto);
        String urlString = ServletUriComponentsBuilder.fromCurrentContextPath().path("votos/votar?id={id}").buildAndExpand(voto.getId()).toString();
        URI uri = new URI(urlString);
        return ResponseEntity.created(uri).body(voto);
    }





}
