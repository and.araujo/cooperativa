package com.cooperativa.dto;

public class SessaoDto {
    private Integer tempoAberta;
    private Integer pautaId;

    public SessaoDto() {
    }

    public SessaoDto(Integer tempoAberta, Integer pautaId) {
        this.tempoAberta = tempoAberta;
        this.pautaId = pautaId;
    }

    public Integer getTempoAberta() {
        return tempoAberta;
    }

    public void setTempoAberta(Integer tempoAberta) {
        this.tempoAberta = tempoAberta;
    }

    public Integer getPautaId() {
        return pautaId;
    }

    public void setPautaID(Integer pautaId) {
        this.pautaId = pautaId;
    }

}
