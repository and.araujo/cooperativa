package com.cooperativa.dto;

public class VotoDto {

    private Integer associadoId;
    private Integer sessaoId;
    private String tipoVotoDescricao;

    public VotoDto() {
    }

    public VotoDto(Integer associadoId, Integer sessaoId, String tipoVotoDescricao) {
        this.associadoId = associadoId;
        this.sessaoId = sessaoId;
        this.tipoVotoDescricao = tipoVotoDescricao;
    }

    public Integer getAssociadoId() {
        return associadoId;
    }

    public void setAssociadoId(Integer associadoId) {
        this.associadoId = associadoId;
    }

    public Integer getSessaoId() {
        return sessaoId;
    }

    public void setSessaoId(Integer sessaoId) {
        this.sessaoId = sessaoId;
    }

    public String getTipoVotoDescricao() {
        return tipoVotoDescricao;
    }

    public void setTipoVotoDescricao(String tipoVotoCodigo) {
        this.tipoVotoDescricao = tipoVotoCodigo;
    }
}
