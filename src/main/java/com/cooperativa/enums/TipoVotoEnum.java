package com.cooperativa.enums;

public enum TipoVotoEnum {
    SIM (1, "Sim"),
    NAO (2, "Não");

    private int codigo;
    private String descricao;

    TipoVotoEnum(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public static TipoVotoEnum getTipoVotoPorDescricao(String descricao) throws Exception {
        if (descricao == null){
            throw new Exception("Só é possível vortar sim ou não");
        }
        for (TipoVotoEnum tve : TipoVotoEnum.values()){
            if( descricao.equals(tve.getDescricao()) ){
                return tve;
            }
        }
        throw  new IllegalArgumentException("Descrição inválida!");
    }

    public static TipoVotoEnum getTipoVoto(Integer codigo){
        if(codigo == null){
            return null;
        }
        for (TipoVotoEnum tve : TipoVotoEnum.values()){
            if (codigo.equals(tve.getCodigo())){
                return tve;
            }
        }
        throw  new IllegalArgumentException("Id inválido!");
    }
}
